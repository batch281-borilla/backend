// JavaScript Operators

// _______Arithmetic Assignment Operators_______

let	x = 1397;
let	y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let	product = x * y;
console.log("Result of multiplication operator: " + product);

let	quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// *** Assignment Operators (=) - assigns the value of the right operand to a variable.
// _______Addition Assignment Operator (+=)_______

let	assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Shorthand
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// *** Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)
assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);
assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);
assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);


// _______Multiple Operators and Parentheses_______

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operator: " + mdas);
// ex. 3 * 4 / 5 = 2.4 ... 1 + 2 = 3 ... 3 - 2.4 = 0.6

// Using Parentheses
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operator: " + pemdas);

pemdas = (1 + (2 - 3)) * (4 / 5);
console.log("Result of pemdas operator: " + pemdas);


// _______Increment and Decrement (++/--)_______
let z = 1;

// * Pre-increment
let	increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// * post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);


// Type Coercion
let numA = '10';
let	numB = 12;

let	coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion)

let numC = true + 1;
console.log(numC);

// _______Comparison Operators_______

let juan = 'juan';

// * Equality Operator (==)
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == juan);

// * Inequality operator (!=)
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != juan);

// * Strict Equality Operator (===)
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === juan);

// * Strict Inequality Operator (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== juan);


//  _______Relational Operators_______
let a = 50;
let	b = 65;

//  Greater than (>)
let	isGreaterThan = a > b;
// Less than (<)
let	isLessThan = a < b;
// Greater than or equal (>=)
let isGTorEqual = a >= b;
// Less than or equal (<=)
let isLTorEqual = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

// Logical Operators - comparison of the result which is 'true' or 'false'
let isLegalAge = true;
let isRegistered = false;

// Logical and Operator (&&)
// Returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// Logical Or Operator (||)
// Returns true if one of the operands are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Logical Not Operator (!)
// Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet);
