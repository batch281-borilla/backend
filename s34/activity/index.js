const express = require("express");
const app = express();

const port = 4000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));


app.get("/home", (req, res) => {
	res.send("Welcome to the home page");
});

const users = [
	{ username: "johndoe",
	  password: "johndoe1234" }
];

app.get("/users", (req, res) => {
	res.send(users);
})


app.delete('/delete-user', (req, res) => {
  const username = req.body.username;
  let message = "";

  if (users.length > 0) {
    for (let i = 0; i < users.length; i++) {
      if (username === users[i].username) {
        users.splice(i, 1);
        message = `User ${username} has been deleted.`;
        break;
      }
    }
  }

  if (message === "") {
    message = "User not found.";
    res.status(404);
  }

  res.send(message);
});







if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`))
}

module.exports = app;