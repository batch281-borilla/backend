const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


// _______Route for creating a product_______
/*
router.post("/", auth.verify, (req, res) => {
	
	const data = {
    product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		productController.createProduct(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

});

*/

// router.get("/", auth.verify, (req, res) => {

//   productController.createProduct().then(resultFromController => res.send(
//     resultFromController));
// });




//------------------------------------------------------------

router.post("/", auth.verify, (req, res) => {
  const data = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    isAdmin: auth.decode(req.headers.authorization).isAdmin, // Get the isAdmin value from the decoded user data
  };

  productController
    .createProduct(data)
    .then((resultFromController) => res.send(resultFromController))
    .catch((error) => {
      res.status(401).send({ error: "Unauthorized access" });
    });
});



// _______Route for retrieving all the products_______
/*
router.get("/all", (req, res) => {

  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  if(data.isAdmin == true){
    productController.getAllProducts(req.params).then(resultFromController => res.send(resultFromController));
  } else {
    res.send(false);
  }

});
*/

router.get("/all", (req, res) => {
  productController.getAllProducts().then(resultFromController => res.send(
    resultFromController));
});



//-----------------------------------------------------------
/*
router.get("/all", auth.verify, (req, res) => {
  // Check if the user is an admin before retrieving all products
  if (!req.user.isAdmin) {
    return res.status(401).send({ error: "Unauthorized access" });
  }

  productController.getAllProducts().then((resultFromController) =>
    res.send(resultFromController)
  );
});
*/
//-----------------------------------------------------------
/*
router.get("/all", auth.verify, (req, res) => {
  
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  if(data.isAdmin == true){
    productController.getAllProducts(data).then(resultFromController => res.send(resultFromController));
  } else {
    res.send(false);
  }

});
*/

// _______Route for retrieving all the "ACTIVE" products_______
router.get("/", (req, res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(
		resultFromController));
});


// _______Route for retrieving a "SPECIFIC" product_______
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(
		resultFromController));
});


// _______Route for "UPDATING" a product_______

router.put("/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController =>
		res.send(resultFromController));
});


//---------------------------------------------------
/*
router.put("/:productId", auth.verify, (req, res) => {
  // Check if the user is an admin before updating the product
  const isAdmin = req.user.isAdmin;
  if (!isAdmin) {
    return res.status(401).send({ error: "Unauthorized access" });
  }

  productController
    .updateProduct(req.params, req.body)
    .then((resultFromController) => {
      if (resultFromController) {
        res.send({ message: "Product updated successfully" });
      } else {
        res.status(400).send({ error: "Product update failed" });
      }
    })
    .catch((error) => {
      res.status(400).send({ error: "Product update failed" });
    });
});
*/



// _______Route for deactivating a product_______


router.put("/:productId/deactivate", auth.verify, (req, res) => {

  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  if(data.isAdmin == true){
    productController.deactivateProduct(req.params).then(resultFromController => res.send(resultFromController));
  } else {
    res.send(false);
  }
  
});



// ----------------------------------------------------------------
/*
router.put("/:productId/deactivate", auth.verify, (req, res) => {
  // Check if the user is an admin before deactivating the product
  const isAdmin = req.user.isAdmin;
  if (!isAdmin) {
    return res.status(401).send({ error: "Unauthorized access" });
  }

  productController
    .deactivateProduct(req.params.productId)
    .then((resultFromController) => {
      if (resultFromController) {
        res.send({ message: "Product deactivated successfully" });
      } else {
        res.status(400).send({ error: "Product deactivation failed" });
      }
    })
    .catch((error) => {
      res.status(400).send({ error: "Product deactivation failed" });
    });
});
*/

// _______Route for reactivating a product_______


router.put("/:productId/reactivate", auth.verify, (req, res) => {

  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  if(data.isAdmin == true){
    productController.reactivateProduct(req.params).then(resultFromController => res.send(resultFromController));
  } else {
    res.send(false);
  }
  
});



//-------------------------------------------------------------
/*
router.put("/:productId/reactivate", auth.verify, (req, res) => {
  // Check if the user is an admin before reactivating the product
  const isAdmin = req.user.isAdmin;
  if (!isAdmin) {
    return res.status(401).send({ error: "Unauthorized access" });
  }

  productController
    .reactivateProduct(req.params.productId)
    .then((resultFromController) => {
      if (resultFromController) {
        res.send({ message: "Product reactivated successfully" });
      } else {
        res.status(400).send({ error: "Product reactivation failed" });
      }
    })
    .catch((error) => {
      res.status(400).send({ error: "Product reactivation failed" });
    });
});
*/




// _______Route to archiving a product_______
/*
router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
	
});
*/




module.exports = router;