const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// ________Route for checking if the email already exists in the database________
// Invokes the checkEmailExists function from the controller file
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// _______Route for user registration_______
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})


// _______Route for authentication_______
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


// _______Route for retrieving user details_______
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


// _______Route to user checkout_______


router.post("/checkout", auth.verify, (req, res) => {
  let data = {
    userData: auth.decode(req.headers.authorization),
    productId: req.body.productId,
    productName: req.body.productName,
    quantity: req.body.quantity,
  };

  userController.orderProduct(data).then(resultFromController => res.send(resultFromController));
});



// _______Route for retrieving user details_______
router.get("/:userId/userDetails", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	userController.getDetails({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});






module.exports = router;