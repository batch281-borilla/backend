const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// _______Check email_______

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		// The find method returns a record if a match is found
		if(result.length > 0){
			return true;

		// No duplicate emails found
		// The user is not registered in the database
		} else {
			return false;
		};
	});
};




// _______User registration________

module.exports.registerUser = (reqBody) => {

	// Creates a variable named "newUser" and instantiates a new "User" object using the Mongoose model
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		// User registration failed
		if(error){
			return false;

		// User registration successful
		} else {
			return true;
		}
	})

};



// _______User authentication_______
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {

		// User does not exist
		if(result == null){
			return false

		// User exists
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result)}

			} else {
				return false;
			}
		}
	})
}


// _______Retrieve user details_______
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		
		result.password = "";
		
		return result;

	});

};


// _______Create order_______



module.exports.orderProduct = async (data) => {
  const product = await Product.findById(data.productId);
  if (!product) {
    return { success: false, message: 'Product not found.' };
  }

  const user = await User.findById(data.userData.id);
  if (!user) {
    return { success: false, message: 'User not found.' };
  }

  const productPrice = product.price;
  product.userOrders.push({ userId: data.userData.id });
  await product.save();

  const quantity = data.quantity;
  const totalAmount = productPrice * quantity;

  user.orderedProduct.push({
    products: [
      {
        productId: data.productId,
        productName: product.name,
        quantity: quantity,
      },
    ],
    totalAmount: totalAmount,
  });
  await user.save();

  return { success: true, message: 'Checkout successful.' };
};





// _______Retrieve user details_______
module.exports.getDetails = (data) => {

	return User.findById(data.userId).then(result => {

		
		result.password = "";

				return result;


	});

};
