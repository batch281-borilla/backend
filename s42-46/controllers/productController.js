const Product = require("../models/Product");


// _______Add/Create products_______
/*
module.exports.addProduct = (data) =>{

	let newProduct = new Product({
		name : data.product.name,
		description : data.product.description,
		price : data.product.price
	});

	return newProduct.save().then((product, error) => {
		if(error){
			return false;

		}else {
			return true;
		};
	})
};
*/

// ----------------------------------------------
/*
module.exports.createProduct = (data) => {
  let newProduct = new Product({
    name: data.product.name,
    description: data.product.description,
    price: data.product.price,
  });

  return newProduct.save().then((product, error) => {
    if (error) {
      return { success: false, message: 'Product creation failed.' };
    } else {
      return { success: true, message: 'Product created successfully.' };
    }
  });
};
*/
//--------------------------------------------------

module.exports.createProduct = (data) => {
  let newProduct = new Product({
    name: data.name,
    description: data.description,
    price: data.price,
  });

  return newProduct.save().then((product, error) => {
    if (error) {
      return { success: false, message: 'Product creation failed.' };
    } else {
      return { success: true, message: 'Product created successfully.' };
    }
  });
};


// _______Retrieve all products_______

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result
	});
};


// ------------------------------------------------

/*
module.exports.getAllProducts = (isAdmin) => {
  // Check if the user is an admin before retrieving all products
  if (!isAdmin) {
    return Promise.reject("Unauthorized access");
  }

  return Product.find({}).then((result) => {
    return result;
  });
};
*/



// _______Retrieve all "ACTIVE" products_______
module.exports.getAllActiveProducts = () => {
	return Product.find({isActive : true}).then(result => {
		return result
	});
};


// _______Retrieving a "SPECIFIC" product_______
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	});
};



// _______"UPDATING" a product_______

module.exports.updateProduct = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

		if(error){
			return false;
		} else {
			return true;
		}
	})

}


//----------------------------------------------------------------

/*
module.exports.updateProduct = (reqParams, reqBody) => {
  return Product.findByIdAndUpdate(
    reqParams.productId,
    {
      $set: {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
      },
    },
    { new: true } // To return the updated product in the response
  )
    .then((product) => {
      return { success: true, product };
    })
    .catch((error) => {
      return { success: false, error };
    });
};
*/



// _______Deactivate a product_______


module.exports.deactivateProduct = (reqParams) => {

  let updateActiveField = {
    isActive : false
  };

  return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

    if (error) {
      return false;
    } else {
      return true;

    }

  });
};



//------------------------------------------------------
/*
module.exports.deactivateProduct = (reqParams) => {
  return Product.findByIdAndUpdate(
    reqParams.productId,
    { isActive: false },
    { new: true } // To return the updated product in the response
  )
    .then((product) => {
      return { success: true, product };
    })
    .catch((error) => {
      return { success: false, error };
    });
};
*/

// _______Reactivate a product_______

module.exports.reactivateProduct = (reqParams) => {

  let updateActiveField = {
    isActive : true
  };

  return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

    if (error) {
      return false;
    } else {
      return true;

    }

  });
};



//------------------------------------------------------
/*
module.exports.reactivateProduct = (reqParams) => {
  return Product.findByIdAndUpdate(
    reqParams.productId,
    { isActive: true },
    { new: true } // To return the updated product in the response
  )
    .then((product) => {
      return { success: true, product };
    })
    .catch((error) => {
      return { success: false, error };
    });
};

*/







// _______Archive a product_______
/*
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {
			return false;
		} else {
			return true;

		}

	});
};
*/