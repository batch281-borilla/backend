const express = require("express");
const mongoose = require("mongoose");

// Connect the frontend to backend
const cors = require("cors"); 

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const app = express();

//_______Connect to our MongoDB_______
mongoose.connect("mongodb+srv://alexismarcnborilla:l39Va5GLT2ojAD2h@wdc028-course-booking.leugb8s.mongodb.net/e-commerce-API-Capstone2?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error',console.error.bind(console,"MongoDB connection Error."));
db.once('open',()=>console.log('Now connected to MongoDB Atlas!'));
// mongoose.connection.once('open',()=>console.log('Now connected to MongoDB Atlas!'));


// _______Allows all resources to access our backend application_______
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// _______Defines the "/users" string to be included for all user routes defined in the "userRoutes" file_______
app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4000,()=>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})