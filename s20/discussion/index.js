

// _______While Loop_______
/*
	- it evaluates a condition, if returned true, it will execute statements as long as the condition is satisfied. 
		If at first the condition is not satisfied, no statement will be executed.

	Syntax:
		while(expression/condition){
			statement/s;
		}
*/

console.log("While loop")

let count = 5;

while(count !== 0) {
	console.log("While: " + count);
	count--;
}
/*
	count: 5, 4, 3, 2, 1, 0
	console:
		while: 5
		While: 4
		While: 3
		While: 2
		While: 1
*/

// Mini activity: Using while loop, display number 1-5
let count3 = 0;

while(count3 !== 5) {
	console.log("While: " + count3);
	count3++;
}
/*
	count: 0, 1, 2, 3, 4, 5
	console:
		while: 0
		While: 1
		While: 2
		While: 3
		While: 4
*/


let count2 = 1;

while(count2 < 6){
	console.log("While: " + count2);
	count2++;
}


// _______Do-while loop_______
/*
	- iterates statements within a number of times based on a condition. However, if the condition was not satisfied at first, one statement will be executed.
	Syntax:
		do{
			statement/s;
		} while(expression/condition);
*/

 /*
 let number = Number(prompt("Give me a number"));

 do{
 	console.log("Do while: " + number);

 	number += 1;
 	// number = number +1;
 }while(number < 10);
 */


console.log("Do-while loop")

let even = 2;

do{
	console.log(even);
	even += 2;
}while(even <= 10);


// _______For loop_______
/*
	- a looping construct that is more flexible than other loops. It consists of three parts:
		1. Initialization
		2. Expression/condition
		3. final expression/step expression

	Syntax:
		for(initialization; expression/condition; finalExpression){
			statement/s;
		}
*/
console.log("For loop")

for(let count = 0; count <= 20; count++){
	console.log(count);
}
/*
	count = 0, 1, 2 .. 20,21
	console:
		0
		1
		2
		.
		.
		20
*/


let myString = "alex";

console.log(myString.length);

// Accessing elements of a string
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

console.log("Looping through array index")
for(let x = 0;x < myString.length;x++){
	console.log(myString[x]);
}


let myName = "AlExIs";

console.log("Looping through vowels and consonants")

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
	){
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}


// _______Continue and Break Statements_______

console.log("Continue and Break Statements");
for(let count = 0; count <= 20; count++){
	if(count % 2 === 0){
		// Tells the code to continue to the next iteration of the loop.
		continue;
	}

	console.log("Continue and Break: " + count);

	// If the current value of count is greater than 10, stops the loop
	if(count > 10){
		// tells the code to terminate/stop the loop even if the condition of the loop defines that it should execute.
		break;
	}
}
// count: 10, 11

let name = "alexandro"

for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	// If the character is equal to 'a', continue to the next iteration
	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	// If the current letter is equal to 'd', stop the loop
	if(name[i] == "d"){
		break;
	}
}
