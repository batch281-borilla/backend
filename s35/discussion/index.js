const express = require("express")
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://alexismarcnborilla:l39Va5GLT2ojAD2h@wdc028-course-booking.leugb8s.mongodb.net/b281_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);
// useNewUrlParser : true,/useUnifiedTopology: true = Depracators
// sila ang nagbibigay ng warning whenever may problem kay MongoDB

// Set notification for connection success or failure
let db = mongoose.connection; // will handle certain error pag nastablish na ang connection

// If a connection error occured, output in the console
db.on("error", console.error.bind(console, "connection error")); //para makapagprint ng error

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));


// _______Mongoose Schema_______
const tasksSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})

// _______Models [Task models]_______
const Task = mongoose.model("Tasks", tasksSchema) //has 2 parameters. 1st kung saan isesave ang created data (Tasks) and 2nd name ng model (tasksSchema)

// Allows the app to read json data
app.use(express.json());
// Allows the app to read data from forms
app.use(express.urlencoded({extended:true}));

// Creation of todo list routes

// _______Creating a new task_______
app.post("/tasks", (req, res) => {
	
	Task.findOne({name : req.body.name}).then((result, err) => {
		// If a document was found and document's name matches the information from the client
		if(result !== null && result.name === req.body.name){
		// Return a message to the client/Postman
		return res.send("Duplicate task found");
		}
		// If no document was found
		else{
			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			});

			newTask.save().then((savedTask, saveErr) => {
				// If there are errors in saving
				if(saveErr){
					return console.error(saveErr);
				}
				// No error found while creating the document
				else{
					return res.status(201).send("New task created");
				}
			})
		}
	})	
})



// _______Get all the tasks________
app.get("/tasks", (req, res) => {
	Task.find({}).then((result, err) => {
		// If an error occured
		if(err){
			// Will print any errors found in the console
			return console.log(err);
		}
		// If no errors are found
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})




// *** s35 ACTIVITY ***

const userSchema = new mongoose.Schema({
	userName : String,
	password : String,
	status : {
		type : String,
		default : "pending"
	}
})


const User = mongoose.model("User", userSchema)

app.use(express.json());

app.use(express.urlencoded({extended:true}));



app.post("/signup", (req, res) => {

	User.findOne({name : req.body.name}).then((result, err) => {
		if(result !== null && result.name === req.body.name){
			return res.send("Duplicate user found");
		}
		else{
			let newUser = new User({
				userName : req.body.name,
				password : req.body.name
			});

			newUser.save().then((savedUser, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New user registered");
				}
			})
		}
	})
})



app.get("/signup", (req, res) => {
	User.find({}).then((result, err) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

 // *** End of Activity ***



// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));