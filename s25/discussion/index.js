// _______JSON Object_______
/*
	- JSON stands for JavaScript Object Notation
	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/

// {
// 	"city": "Quezon city",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// _______JSON Arrays_______
// "cities": [
// 	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"},
// 	{"city": "Manila city", "province": "Metro Manila", "country": "Philippines"},
// 	{"city": "Makati city", "province": "Metro Manila", "country": "Philippines"}
// ]

// Mini Activity - Create a JSON Array that will hold three breeds of dogs with properties: name, age, breed.

// "dogs": [
// 	{"name": "Bella", "age": "3", "breed": "Labrador Retriever"},
// 	{"name": "Max", "age": "5", "breed": "German Shepherd"},
// 	{"name": "Daisy", "age": "2", "breed": "Golden Retriever"}
// ]


// JSON Methods

/*
 _______Convert Data into Stringified JSON_______
  - it is a javascript object that converted to JSON Data to send and receive data into JSON format
*/
let batchesArr = [{ batchName: 'Batch X'}, { batchName: 'Batch Y' }];

// the "stringify" method is used to convert JavaScript objects into a stringified JSON
console.log("Result from stringify method");
console.log(JSON.stringify(batchesArr));


let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
});

console.log(data)


// _______Using Stringify method with variables_______
// User details
let firstName = prompt('Please enter your first name?');
let lastName = prompt('Please enter your last name?');
let age = prompt('Please enter your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city address belong to?')
};


let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData)

// Mini Activity - Create a JSON data that will accept user car details with variable brand, type, year.

 let brand = prompt('Please enter your car brand?');
 let type = prompt('Please enter your car type?');
 let year = prompt('Please enter the manifacture year?');


 let carData = JSON.stringify({
 	brand: brand,
 	type: type,
 	year: year,
 })

 console.log(carData)


// _______Converting Stringified JSON into JavaScript objects (PARSE)_______
// - may mga pumapasok na naka stringify JSON at need iconvert into javascript objects para mas mabasa ng application

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log('Result from parse method');
console.log(JSON.parse(batchesJSON));