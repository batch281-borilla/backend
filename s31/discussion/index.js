// using "require" directive to load http module
let http = require("http"); // http - hypertext protocol - to transfer data from one app to another.
             					  
// Create a server
http.createServer(function (request, response) {
	// Returning what type of response being thrown to the client
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// send the response with the text content 'Hello, world!'
	response.end('Hello, world!');
}).listen(4000)
// parameter ang gagamitin para magrequest kay client at mag response sa client.
// writeHead method - dito ilalagay ang certain details / what type of response ang ibato sa client.
// listen method (port) - para dun makikinig ang server

// When serving is running, console will print the message:
console.log('Server is running at localhost:4000');
// *** then go to terminal and type- node index.js 
// *** go to the browser and type in search bar- localhost:4000











