const http = require('http');

// Creates a variable "port" to store the port number
const port = 4000;

const app = http.createServer((request, response) => {
	// Accessing the "greeting" route returns a message of "Hello, world!"
	if(request.url === '/greeting'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Hello Again');
	}
	else if(request.url == '/homepage'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('This is the homepage');
	}
	else{
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('Page not available');
	}
});

app.listen(port);

console.log(`Server now accessible at localhost:${port}.`);

// npm install -g nodemon -- ininstall sa terminal para kung may babaguhin ay sumasabay na sa server
// nodemon routes.js  - type sa terminal to run the server