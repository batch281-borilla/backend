// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
// Initialize/add the given object properties and methods
// Properties
// Methods

let trainer = {
  name: "Ash Ketchum",
  age: 10,
  pokemon : ["Pikachu", "Charizard", "Squirtle", "Bulbasaur" ],
  friends: {
    name : ["May", "Max","Brock", "Misty"],
  }, 
   talk: function() {
    console.log("Pikachu! I choose you!")
  }
}
// Check if all properties and methods were properly added
// Access object properties using dot notation
// Access object properties using square bracket notation
// Access the trainer "talk" method
console.log(trainer)
console.log("Result of dot notation:")
console.log(trainer.name)
console.log("Result of square bracket notation:")
console.log(trainer['pokemon'])
console.log("Result of talk method:")
trainer.talk()


// Create a constructor function called Pokemon for creating a pokemon



// Create/instantiate a new pokemon
let myPokemon = {
  name: "Pikachu",
  level: 3,
  health: 100,
  attack: 50,
  tackle: function() {
    console.log("This Pokemon tackled targetPokemon");
    console.log("targetPokemon's heath is now reduced to _targetPokemonHealth_");
  },
  faint: function(){
    console.log("Pikachu fainted");
  }
}
console.log(myPokemon);



// Create/instantiate a new pokemon
let Pokemon2 = {
  name: "Geodude",
  level: 8,
  health: 16,
  attack: 8,
  tackle: function() {
    console.log("Geodude tackled Pikachu");
    console.log("Pikachu's heath is now reduced to " + 16);
  },
  faint: function(){
    console.log("Geodude fainted");
  }
}
console.log(Pokemon2);


// Create/instantiate a new pokemon

let Pokemon3 = {
  name: "Mwetwo",
  level: 100,
  health: 200,
  attack: 100,
  tackle: function() {
    console.log("Mwetwo tackled Geodude");
    console.log("Geodude's heath is now reduced to " + -84);
  },
  faint: function(){
    console.log("Mwetwo fainted");
  }
}
console.log(Pokemon3);
// Invoke the tackle method and target a different object
Pokemon2.tackle()



// Invoke the tackle method and target a different object
Pokemon3.tackle()
Pokemon2.faint()







//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}