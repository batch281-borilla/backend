const express = require("express");
const mongoose = require("mongoose");

// way to connect the frontend to backend to communicate properly
const cors = require("cors"); // Cross-Origin Resource Sharing

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();

//_______Connect to our MongoDB_______
mongoose.connect("mongodb+srv://alexismarcnborilla:l39Va5GLT2ojAD2h@wdc028-course-booking.leugb8s.mongodb.net/s37-41API?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error',console.error.bind(console,"MongoDB connection Error."));
db.once('open',()=>console.log('Now connected to MongoDB Atlas!'));


// _______Allows all resources to access our backend application_______
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// _______Defines the "/users" string to be included for all user routes defined in the "userRoutes" file_______
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(process.env.PORT || 4000,()=>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})