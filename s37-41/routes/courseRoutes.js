const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

/*
// Route for creating a course
router.post("/", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(
		resultFromController))
})
*/



// *************** s37-41 (part3) Activity ***************
/*
// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin) {
		courseController.addCourse(req.body).then((result) => {
			res.send(result);
		});
	} else {
		res.send({ auth: "failed"});
	}
});
*/

// _______Sir Vasco's version_______
// Route for creating a course
router.post("/", auth.verify, (req, res) => {
	
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

});

// **************************************************


// _______Route for retrieving all the courses_______
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(
		resultFromController));
});


// _______Route for retrieving all the "ACTIVE" courses_______
router.get("/", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(
		resultFromController));
});


// _______Route for retrieving a "SPECIFIC" course_______
// Creating a course using "/:parametername" creates a dynamic route, meaning the url changes depending on the information provided
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request
	courseController.getCourse(req.params).then(resultFromController => res.send(
		resultFromController));
})


// _______Route for "UPDATING" a course_______
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController =>
		res.send(resultFromController));
});



// ************** s37-41 (part4) Activity **************
/*
// Route to archiving a course
router.patch("/:courseId", auth.verify, (req, res) => {
	
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

});
*/

// _______Sir Vasco's version_______

// Route to archiving a course
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:courseId/archive", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
	
});

// ***********************************************


// Allow us to export the "router" object that will be accessed in our index.js file
module.exports = router;
