const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// ________Route for checking if the email already exists in the database________
// Invokes the checkEmailExists function from the controller file
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// _______Route for user registration_______
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// _______Route for authentication_______
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


// ************ s37-41 (part2) ACTIVITY ************
/*
// Route for retrieving user details
 router.post("/details", (req, res) => {

 	// Provides the user's ID for the getProfile controller method
 	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
 });
*/

// ____Sir Vasco's version____
/*
router.post("/details", (req, res) => {
	
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

});
     *****************************************
*/

// _______Route for retrieving user details_______
// The "auth.verify" acts as middleware to ensure that the user is logged in before they can enroll to a course
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// _______Route to enroll user to a course_______
/*
router.post("/enroll", (req, res) => {

	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})
*/

// ************ s37-41 (part5) ACTIVITY ************

/*
router.post("/enroll", auth.verify, (req, res) =>{
    const data = {
    	
    	isAdmin: auth.decode(req.headers.authorization).isAdmin,

        userId: auth.decode(req.headers.authorization).id,
        courseId: req.body.courseId
    }
    if(data.isAdmin == true){
        userController.enroll(data).then(resultFromController => res.send(resultFromController));
    }else {
        res.send(false)
    }

});

_____________________________________
*/

router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userData : auth.decode(req.headers.authorization),
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})



module.exports = router;