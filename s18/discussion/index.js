// _______Function with Parameters_______

function printName(name){
	console.log("My name is " + name);
};

printName("Juana");
printName("John");

let sampleVariable = "Bella";
printName(sampleVariable);

// _______________________________

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
};

checkDivisibilityBy8(64);

// _______Functions as Arguments_______
function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed.");
};

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);

// _______Using multiple parameters_______
function createFullName(firstName, middleName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
};

createFullName('Juan', 'Perez', 'Dela Cruz');

// _______Using variable as arguments _______
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// _______Return Statement - allows us to output a value from a function to be passed to the line/block of code that invoked the function._______

function returnFullName(firstName, middleName, lastName){
	return firstName + ' ' + middleName + ' ' + lastName;
	console.log("This message will not be printed.");
}

// returnFullName("Jeffrey", "Smith", "Bezos"); - hindi mapprint dahil sa "return" statement.. si "return" statement ang gumagawa ng makikita nating sturcture pero hindi madidisplay

let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);
console.log(returnFullName(firstName, middleName, lastName));

// __________________________________

function returnAddress(city, country){
	let fullAddress = city + ', ' + country;
	return fullAddress;
}

let myAddress = returnAddress("Cebu city" , "Philippines");
console.log(myAddress);