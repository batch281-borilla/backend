// ***[Section] Comparison Query Operators

// _______$gt/$gte_______
/*
	- Allows us to find documents that have field number values greater than or equal to a specified field
	Syntax:
		db.collectionName.find({fields: $gt: value });
		db.collectionName.find({fields: $gte: value });
*/
db.users.find({ age: {$gt: 50 }});
db.users.find({ age: {$gte: 20 }});

// _______$lt/$lte_______
/*
	- Allows us to find documents that have field number values less than or equal to a specied field
	Syntax:
		db.collectionName.find({field: $lt: value });
		db.collectionName.find({field: $lte: value });
*/
db.users.find({ age: { $lt: 50 }});
db.users.find({ age: { $lte: 65 }});

// _______$ne operator_______
/*
	- Allows us to find documents that have field numbers not equal to specified value.
	Syntax:
		db.collectionName.find(field: {$ne: value });
*/
db.users.find({ age: { $ne: 82 }});


// _______$in operator________
/*
	- Allows us to find documents with specific match criteria one field using different values.
	Syntax:
		db.collectionName.find(field: {$in: value });
*/
db.users.find({ lastName: { $in: ["Hawking", "Doe"] } } );
db.user.find({ courses: { $in: ["HTML", "React"] } } );


// ______Opposite of in is not in, $nin_______
db.users.find({ courses: { $nin: ["HTML", "React"] } } );

// ***[Section] Logical Query Operators

// ________$or operator_______
/*
	- Allows us to find documents that match a single criteria from multiple provided search criteria
	Syntax:
		db.collectors.find({ $or: { fieldA: valueB }, {fieldB: valueC} });
*/
db.users.find({$or: [{firstName: "Neil"}, {age: 21}] });
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30} }] });


// _______$and operator________
/*
	- Allows us to find documents matching multiple criteria in a single field
*/
db.users.find({$and: [{age: {$ne:82}}, {age: {$ne:76}}]});


// ***Field Projection
/*
	- Retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response.
	- When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.
	- To help with readability of the values returned, we can include/exclude fields from the response.
*/

// _______Inclussion_______
/*
	- Allows us to include/add specific fields only when retrieving documents.
	- The value provided is 1 to denote that the field is being included.
	Syntax:
		db.collectionName.find({criteria, {field: 1}})
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

// _______Exclussion_______
/*
	- Allows us to exclude/remove specific fields only when retrieving documents.
	- The value provided is 0 to denote that the field is being excluded.
	Syntax:
		db.collectionName.find({criteria, {field: 0}})
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		contact: 0,
		department: 0
	}
);


// _______Supressing the ID field_______
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
);

// Returning specific fields in Embedded documents
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
);

// Supressing specific fields in embedded documents
db.users.find(
	{
		firstName: "Jane"
	},
	{
		"contact.phone": 0
	}
)


// Project Specific Array Elements in Returned Array
// The $slice operator allows us to retrieve only 1 element that matches the search criteria

db.users.insertOne({
    namearr: [
        {
            fname: "juan",
        },
        {
            fname: "juan",
        }
    ]
});

db.users.find(
	{ "namearr": 
		{ 
			fname: "juan" 
		} 
	}, 
	{ namearr: 
		{ $slice: 2 } 
	}
);

// [Section] Evaluation Query Operators

// $regex
/*
	- Allows us to find documents that match a specific string pattern using regular expressions
	- search specific letter that exist in a field value, sequence of characters/ string pattern.
	- Syntax:
	db.users.find({field: $regex: 'pattern', $options: 'optionValue'})
*/

// Case sensitive query
db.users.find({firstName: { $regex: 'N'}})
db.users.find({firstName: { $regex: 'n'}})
db.users.find({firstName: { $regex: 'Steph'}})
//   .         .
/* Jane, Stephen*/ 

// Case insensitive query
db.users.find({firstName: { $regex: 'j', $options: 'i'}})
db.users.find({firstName: { $regex: 'N', $options: 'i'}})


