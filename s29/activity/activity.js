/*
Find users with letter s in their first name or d in their last name.
- Use the $or operator.
- Show only the firstName and lastName fields and hide the _id field.
*/

db.users.find({ $or: [{ firstName: { $regex: "s", $options: 'i'} }, {lastName: { $regex: "d", $options: 'i'} } ] }, {
		contact: 0,
		department: 0,
        course: 0,
        age: 0,
        _id: 0
	} );




/*
Find users who are from the HR department and their age is greater than or equal to 70.
- Use the $and operator
*/

db.users.find({ $and: [{ department: "HR" }, { age: { $gte:70 } } ] } );




/*
 Find users with the letter e in their first name and has an age of less than or equal to 30.
- Use the $and, $regex and $lte operators
*/


db.users.find({ $and: [{ firstName: { $regex: "e" } }, { age: { $lte:70 } } ] } );

