//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo(){

    return await (

       //add fetch here.
       
       fetch('<urlSample>')
       .then((response) => response.json())
       .then((json) => json)
   
   
   );

}


// Getting all to do list item
async function getAllToDo(){

   return await (

      //add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos')
      .then((response) => response.json())
      .then((json) => json)

  );

   const titles = data.map(item => item.title);
   return titles;

}

// to print the result to see the expected output 
async function printAllToDoTitles() {
  const titles = await getAllToDo();
  console.log(titles);
}

printAllToDoTitles();



// [Section] Getting a specific to do list item
async function getSpecificToDo(){
   
   return await (

       //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos/1')
      .then((response) => response.json())
      .then((json) => json)

   );

}

// to print the result to see the expected output 
async function printSpecificToDo() {
  const todo = await getSpecificToDo();
  console.log(todo);
}

printSpecificToDo();


// [Section] Creating a to do list item using POST method
async function createToDo() {
  const todoItem = {
    title: 'Created To Do List Item',
    userId: 1,
    completed: false,
  };

  const response = await fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(todoItem),
  });

  const data = await response.json();

  return data;
}



// to print the result to see the expected output
async function printCreatedToDo() {
  const createdTodo = await createToDo();
  console.log(createdTodo);
}

printCreatedToDo();



// // [Section] Updating a to do list item using PUT method
function updateToDo() {
  const todoItem = {
    title: 'Updated To Do List Item',
    description: 'To update the my to do list with a difference data structure',
    status: 'Pending',
    dateCompleted: 'Pending',
    userId: 1,
  };

  return fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(todoItem),
  })
    .then(response => response.json())
    .then(data => {
      return data;
    });
}


// to print the result to see the expected output
async function printUpdatedToDo() {
  const updatedTodo = await updateToDo();
  console.log(updatedTodo);
}

printUpdatedToDo();




//[Section] Deleting a to do list item
async function deleteToDo() {
  const response = await fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'DELETE',
  });

  return response;
}

// to print the result to see the expected output
async function printDeleteResult() {
  const deleteResponse = await deleteToDo();
  console.log(deleteResponse);
}

printDeleteResult();






//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}


