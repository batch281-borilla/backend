
// _______Array Traversal_______

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lonovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

let mixedArr = [12, 'Asus', null, undefined];  /*This is not recommended.*/

let tasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];

// Creating an array with values from variables.
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Mumbai";

let cities = [city1, city2, city3];

console.log(tasks)
console.log(cities)


// _______Array length property_______
console.log("Array length");
console.log(tasks.length);
console.log(cities.length);

let fullName = "Aizaac Estiva";
console.log(fullName.length);

tasks.length = tasks.length - 1;
console.log(tasks.length);
console.log(tasks);

cities.length--;
console.log(cities);

fullName.length = fullName.length - 1;
console.log(fullName.length);


// _______Adding a number to lengthen the size of the array_______

let theBeatles = ["John", "Paul", "Ringo", "George",];
theBeatles.length++;
console.log(theBeatles);


/*
_______Accessing elements of an array_______ 
given this variables: 
"let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lonovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
*/

console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[20]); /* undefined - dahil walang item na ganito*/

// mini activity: access the second item in the array & the fourth item in the array
let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];

console.log(lakersLegends[1])
console.log(lakersLegends[3])

let currenLakers = lakersLegends[2];
console.log("Accessing arrays using variables");
console.log(currenLakers);

// * Reassigning values in array_______
console.log("Array before reassignment:");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("After reassignment:");
console.log(lakersLegends)


// _______Accessing the last element of an array_______
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length - 1;

console.log(bullsLegends[lastElementIndex]);

// Directly access the expression
console.log(bullsLegends[bullsLegends.length - 1]);



// _______Adding items into the array_______
console.log("Adding items in array");

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);


// _______Looping through array_______
// computerBrands = ['Acer', 'Asus', 'Lonovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
for(let index = 0; index < computerBrands.length; index++){
	console.log(computerBrands[index]);
}

// Check the element if it's divisible by 5

let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0) {
		console.log(numArr[index] + " divisible by 5");
	}
	else{
		console.log(numArr[index] + " is not divisible by 5");
	}
}


// _______Multidimensional array_______
// 
/*
	2 x 2 Two dimensional array
	let twoDim = [[elem1, elem2],[elem3, elem4]]
					0 		1 		0 		1
						0 				1
	twoDim[0][0]; - access element 1
	twoDim [1][0]; - access element 3
*/

let twoDim = [['Kenzo', 'Alonzo'],['Bella', 'Aizaac']];

console.log(twoDim[0][1]); /* to access Alonzo*/
console.log(twoDim[1][0]); /* to access Bella*/
console.log(twoDim[1][1]); /* to access Aizaac*/
